defmodule MatchedWeb.LocalePlug do
  @moduledoc """
  This module defines a plug for automatically choosing the locale to be used for each user.
  """
  # Copied from
  # https://dev.to/wintermeyer/i18n-with-phoenix-liveview-28mj
  # For a version with cookies and user-language selection, see:
  # https://www.paulfioravanti.com/blog/internationalisation-phoenix-liveview/
  alias Plug.Conn
  @behaviour Plug

  @impl Plug
  def init(_opts), do: nil

  @impl Plug
  def call(conn, _opts) do
    accepted_languages = extract_accept_language(conn)
    known_locales = Gettext.known_locales(MatchedWeb.Gettext)

    accepted_languages =
      Enum.filter(accepted_languages, fn l -> Enum.member?(known_locales, l) end)

    locale =
      case accepted_languages do
        [locale | _] ->
          Gettext.put_locale(MatchedWeb.Gettext, locale)
          locale

        _ ->
          Gettext.get_locale()
      end

    conn |> Conn.put_session(:locale, locale)
  end

  # Copied from
  # https://raw.githubusercontent.com/smeevil/set_locale/fd35624e25d79d61e70742e42ade955e5ff857b8/lib/headers.ex
  defp extract_accept_language(conn) do
    case Conn.get_req_header(conn, "accept-language") do
      [value | _] ->
        value
        |> String.split(",")
        |> Enum.map(&parse_language_option/1)
        |> Enum.sort(&(&1.quality > &2.quality))
        |> Enum.map(& &1.tag)
        |> Enum.reject(&is_nil/1)
        |> ensure_language_fallbacks()

      _ ->
        []
    end
  end

  defp parse_language_option(string) do
    captures = Regex.named_captures(~r/^\s?(?<tag>[\w\-]+)(?:;q=(?<quality>[\d\.]+))?$/i, string)

    quality =
      case Float.parse(captures["quality"] || "1.0") do
        {val, _} -> val
        _ -> 1.0
      end

    %{tag: captures["tag"], quality: quality}
  end

  defp ensure_language_fallbacks(tags) do
    Enum.flat_map(tags, fn tag -> remove_country_variant(String.split(tag, "-"), tags) end)
  end

  defp remove_country_variant(tag = [language, _country_variant], tags) do
    if Enum.member?(tags, language), do: [tag], else: [tag, language]
  end

  defp remove_country_variant(tag, _tags), do: tag
end
