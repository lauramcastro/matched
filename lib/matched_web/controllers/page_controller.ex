defmodule MatchedWeb.PageController do
  use MatchedWeb, :controller

  def index(conn, _params) do
    render(conn, "index.html")
  end
end
