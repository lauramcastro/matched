defmodule MatchedWeb.JudgeController do
  use MatchedWeb, :controller

  alias Matched.Panels

  def index(conn, _params) do
    render(conn, "index.html", judges: Panels.list_judges())
  end

  def show(conn, %{"id" => id}) do
    render(conn, "show.html", judge: Panels.get_judge(id))
  end
end
