defmodule MatchedWeb.ProjectController do
  use MatchedWeb, :controller

  alias Matched.Projects

  def index(conn, _params) do
    render(conn, "index.html", projects: Projects.list_projects())
  end

  def show(conn, %{"id" => id}) do
    render(conn, "show.html", project: Projects.get_project(id))
  end
end
