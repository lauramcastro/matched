defmodule Matched.Repo do
  use Ecto.Repo,
    otp_app: :matched,
    adapter: Ecto.Adapters.Postgres
end
