defmodule Matched.Panels.Judge do
  @moduledoc """
  Defines the data structure for representing a judge.
  """
  # other traits of interest: sex, served, group
  defstruct [:id, :name, :availability, :concentration]
end
