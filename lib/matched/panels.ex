defmodule Matched.Panels do
  @moduledoc """
  The Panels context.
  """

  alias Matched.Panels.Judge

  def list_judges do
    [
      %Judge{id: "1", name: "Laura Castro", availability: false, concentration: "ES"},
      %Judge{id: "2", name: "Beatriz Pérez", availability: true, concentration: "C"},
      %Judge{id: "3", name: "María Martínez", availability: false, concentration: "TI"},
      %Judge{id: "4", name: "Susana Ladra", availability: true, concentration: "SI"},
      %Judge{id: "5", name: "Adriana Dapena", availability: true, concentration: "EC"}
    ]
  end

  def get_judge(id) do
    Enum.find(list_judges(), fn map -> map.id == id end)
  end

  def get_judge_by(params) do
    Enum.find(list_judges(), fn map ->
      Enum.all?(params, fn {key, val} -> Map.get(map, key) == val end)
    end)
  end
end
