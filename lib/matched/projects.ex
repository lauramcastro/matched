defmodule Matched.Projects do
  @moduledoc """
  The Projects context.
  """

  alias Matched.Projects.Project

  def list_projects do
    [
      %Project{
        id: "1",
        title: "A very efficient ML-based compiler",
        student: "Grace Hopper",
        concentration: "C"
      },
      %Project{
        id: "2",
        title: "A large-scale Big Data distributed analyzer",
        student: "Annette Bieniusa",
        concentration: "SI"
      },
      %Project{
        id: "3",
        title: "An IoT-oriented security architecture",
        student: "Katie Moussouris",
        concentration: "EC"
      },
      %Project{
        id: "4",
        title: "A web-based integration of tests-as-infrastructure",
        student: "Huiqing Li",
        concentration: "TI"
      },
      %Project{
        id: "5",
        title: "A space-first methodology for requirements elicitation",
        student: "Margaret Hamilton",
        concentration: "ES"
      }
    ]
  end

  def get_project(id) do
    Enum.find(list_projects(), fn map -> map.id == id end)
  end

  def get_project_by(params) do
    Enum.find(list_projects(), fn map ->
      Enum.all?(params, fn {key, val} -> Map.get(map, key) == val end)
    end)
  end
end
