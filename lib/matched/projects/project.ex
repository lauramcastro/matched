defmodule Matched.Projects.Project do
  @moduledoc """
  Defines the data structure for representing a project.
  """
  # other traits of interest: tutor
  defstruct [:id, :title, :student, :concentration]
end
