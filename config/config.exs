# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :matched,
  ecto_repos: [Matched.Repo]

# Configures the endpoint
config :matched, MatchedWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "/M1E0bMbUS6VA60GPw1KsStAdfZ8qu7RJ541jqqDmfNTeJggznoIlRUtcbbpEJ5W",
  render_errors: [view: MatchedWeb.ErrorView, accepts: ~w(html json), layout: false],
  pubsub_server: Matched.PubSub,
  live_view: [signing_salt: "YbPJufxG"]

# Configures gettext
config :matched, MatchedWeb.Gettext,
  default_locale: "gl",
  locales: ~w(en gl)

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
