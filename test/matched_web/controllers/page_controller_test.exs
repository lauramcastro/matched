defmodule MatchedWeb.PageControllerTest do
  use MatchedWeb.ConnCase
  import MatchedWeb.Gettext

  test "GET /", %{conn: conn} do
    conn = get(conn, "/")
    assert html_response(conn, 200) =~ gettext("Welcome to MatchED")
  end
end
