# MatchED

Aplicación web en Elixir/Phoenix para realizar o proceso de asignación automática de tribunais de Traballos fin de Grao (TFG).

## Uso da aplicación

Dado que o proxecto é unha aplicación Elixir/Phoenix, despois de descargar ou clonar este repositorio, o servidor pode arrincarse do seguinte xeito:

  * Instalando as dependencias mediante `mix deps.get`
  * Inicializando os diferentes compoñentes `mix setup`
  * Arrincando o servidor Phoenix mediante `mix phx.server`

Nese momento debe poder visitarse o enderezo [`localhost:4000`](http://localhost:4000) desde calquera navegador.

Para a instalación de Erlang/Elixir e PostgreSQL (prerrequisitos do sistema), pode consultarse o [sitio web de Phoenix](https://hexdocs.pm/phoenix/installation.html).
